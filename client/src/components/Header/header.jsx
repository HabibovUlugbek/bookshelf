import FontAwesome from "react-fontawesome";
import {Link} from "react-router-dom"
import React, { Component } from 'react'

import Sidenav from 'sidenavjs'
import SideNavItems from "./sidenav_items";

export default class Nav extends Component {
  constructor(props) {
    super(props)
   
    this.state = {
      sidebarOpen: false
    }
  }

  onSetSidebarOpen = open => {
    this.setState({ sidebarOpen: open })
  }

  render () {
    return (
      <Sidenav 
        options={{
          sidenavStyle:{
            background:"#242424",
            maxWidth:"420px",
            width:"240px"
          }}}
        open={this.state.sidebarOpen}
        onSetOpen={this.onSetSidebarOpen}
        sidenav={<SideNavItems />}
        
      >
            <div>
                <header>
                  
                    <div className="open_nav">
                        <FontAwesome name="bars"
                        onClick={() => { this.onSetSidebarOpen(true) }}
                            style={{
                                color:"#ffffff",
                                padding:"10px",
                                cursor:"pointer"
                            }}
                        />
                    </div>
                    <Link to="/" className="logo" >
                        The Book Shelf
                    </Link>
                    
                </header>
            </div>
      </Sidenav>
    )
  }
}