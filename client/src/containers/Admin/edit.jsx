import React, { useState } from 'react'
import { useEffect } from 'react'
import { connect } from 'react-redux'
import { Link ,useParams,useNavigate } from 'react-router-dom'
import { getBook , updateBook, clearBook, deleteBook} from "../../actions"


const EditReview = (props) => {
    let { userId } = useParams();
    const [formData, setFormData] = useState({
    _id:userId,
    name:'',
    author:"",
    review:"", 
    pages:"",
    rating:"",
    price:""
})
let book = props?.books?.book
    useEffect(() => {
       props.dispatch(getBook(userId))
       setRedirect(false)
    }, [])

    let navigate = useNavigate();
    const [redirect, setRedirect] = useState(false);
    useEffect(() => {
    if (redirect){
        return navigate("/");
    }
    },[redirect]);
   
    useEffect(() => {
            setFormData({...formData, ...book})
    }, [book])
    const submitForm = (e) => {
        e.preventDefault()
        props.dispatch(updateBook(formData))
        props.dispatch(clearBook());
        setRedirect(true);
    }

    const deletePost = () => {
        props.dispatch(deleteBook(userId))
        props.dispatch(clearBook());
        setRedirect(true)
    }
    let books = props.books
    return (
        <div className='rl_container article'>
            {/* {
                    books.updateBook ?  
                        <div className="edit_confirm">
                            post updated, <Link to={`/books/${books?.book._id}`} >Click here to see your post</Link>
                        </div>
                    :null
                }
            {
                books.postDeleted ?  
                    <div className="red_tag">
                        Post Deleted 
                    </div>
                :null
            } */}
            <form onSubmit={submitForm}>
                <h2>Edit Reivew</h2>

                <div className='form_element'>
                    <input 
                    type="text" 
                    placeholder='Enter name'
                    value={formData.name}
                    onChange={(e) => setFormData({...formData,name:e.target.value})} />
                </div>

                <div className='form_element'>
                    <input 
                    type="text" 
                    placeholder='Enter author'
                    value={formData.author}
                    onChange={(e) => setFormData({...formData,author:e.target.value})} />
                </div>

                <textarea  
                value={formData.review}
                onChange={(e) => setFormData({...formData,review:e.target.value})}
                />
                

                <div className='form_element'>
                    <input 
                    type="number" 
                    placeholder='Enter pages'
                    value={formData.pages}
                    onChange={(e) => setFormData({...formData,pages:e.target.value})} />
                </div>

                <div className='form_element'>
                   <select
                   value={formData.rating}
                   onChange={(e) => setFormData({...formData,rating:e.target.value})} 
                   >
                       <option val={1}>1</option>
                       <option val={2}>2</option>
                       <option val={3}>3</option>
                       <option val={4}>4</option>
                       <option val={5}>5</option>

                   </select>
                </div>

                <div className='form_element'>
                    <input 
                    type="number" 
                    placeholder='Enter price'
                    value={formData.price}
                    onChange={(e) => setFormData({...formData,price:e.target.value})} />
                </div>


                <button type="submit">Edit review</button>
                <div className='delete_post'>
                    <div className="button" onClick={deletePost}>
                        Delete review
                    </div>
                </div>
            </form>
        </div>
    )
}

function mapStateToProps(state){
    return {
        books:state.books
    }
}

export default connect(mapStateToProps)(EditReview);
