import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { loginUser} from "../../actions"
import { useNavigate } from 'react-router-dom';



const Login = (props) => {

    let navigate = useNavigate();
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [error, setError] = useState('')
    const [success, setSuccess] = useState(false)

    useEffect(() => {
        if(props.user.login?.isAuth){
            navigate("/user")
        }
    }, [props])

    const submitForm = (e) => {
        e.preventDefault();
        let state={
            email,
            password,
            error,
            success
        }
        props.dispatch(loginUser(state))
    }

    let user = props.user

    return (
        <div className="rl_container">
            <form onSubmit={submitForm}>
                <h2>Log in here</h2>
                <div className="form_element">
                    <input 
                    type="email"
                    placeholder="Enter your Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)} />
                </div>

                <div className="form_element">
                    <input 
                    type="password"
                    placeholder="Enter your password" 
                    value={password}
                    onChange={(e) => setPassword(e.target.value)} />
                </div>

                <button type="submit">Log in</button>
                <div className="error">
                    {
                        user.login ? 
                            <div>{user.login.message}</div>
                        :null
                    }
                </div>
            </form>
        </div>
    )
}

function mapStateToProps(state){
    return {
        user:state.user
    }
}

export default connect(mapStateToProps) (Login);
