const jwt = require("jsonwebtoken");
const { User} = require("./../models/user")
const config = require("../config/config").get(process.env.NODE_ENV);

let auth = (req,res, next) => {

    let token = req.cookies.auth;

    jwt.verify(token , config.SECRET, (err, decode) => {
        if(err) return res.status(400).send(err)
        User.findOne({"_id":decode, "token":token} , (err , user) => {
            if(err) return res.status(400).send(err)

            if(!user) return res.json({
                error: true
            })

            req.token = token;
            req.user = user;
            next();
        })
    })
   
}

module.exports = {auth}