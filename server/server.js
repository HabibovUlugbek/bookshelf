const mongoose = require("mongoose")
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const config = require("./config/config").get(process.env.NODE_ENV);
const app = express();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken")

mongoose.Promise = global.Promise;
mongoose.connect("mongodb+srv://habibov:js1234@bookshelf.zini4.mongodb.net/books?retryWrites=true&w=majority")


const {User} = require("./models/user");
const { Book } = require('./models/book')
const { auth } = require("./middleware/auth")

app.use(bodyParser.json());
app.use(cookieParser())

app.use(express.static('client/build'))

// === GET === //

app.get("/api/auth",auth,  (req,res) => {
    res.json({
        isAuth:true,
        id:req.user._id,
        email:req.user.email,
        name:req.user.name,
        lastname:req.user.lastname
    })
})

app.get("/api/logout", auth , (req,res) => {
    var user = req.user;

    user.updateOne({$unset:{token:1}},(err,user ) => {
        if(err) return res.status(400).send(err)

        res.sendStatus(200)
    })
})

app.get("/api/getBook" , (req,res) => {
    let id = req.query.id 

    Book.findById(id , (err, doc) => {
        if(err) return res.status(400).send(err)

        res.send(doc)
    })
})


app.get("/api/books" , (req,res) => {
    //localhost:3001/api/books?skip=3&limit=3&order=desc
    let skip = parseInt(req.query.skip)
    let limit = parseInt(req.query.limit)
    let order = req.query.order;


    //order =  desc || asc
    Book.find().skip(skip).sort({_id:order}).limit(limit).exec((err, doc) => {
        if(err) return res.status(400).send(err)

        res.send(doc)
    })
})

app.get("/api/getReviewer", (req,res) => {
    let id = req.query.id;
    
    User.findById(id , (err, user) => {
        if(err) return res.status(400).send(err)
        res.json({
            name:user.name,
            lastname:user.lastname
        })
    })
})

app.get("/api/users", (req,res) => {
    User.find({},(err , users) => {
        if(err) return res.status(400).send(err)

        res.status(200).send(users)
    } )
});

app.get("/api/user_posts", (req,res) => {
    Book.find({ownerId:req.query.user}).exec((err,docs) => {
        if(err) return res.status(400).send(err)

        res.send(docs)        
    })
})

// === POST === //
app.post("/api/book" , (req,res) => {
    const book = new Book(req.body)

    book.save((err, doc) => {
        if(err) return res.status(400).send(err)
        res.status(200).json({
            post:true,
            bookId: doc._id
        })
    })
})

app.post("/api/register" , (req,res) => {
    const user = new User(req.body);

    user.save((err, doc) => {
        if(err) return res.json({success: false,err: err})

        res.status(200).json({
            success:true,
            user:doc
        })
    })
})

app.post("/api/login", (req,res) => {
    const { password } = req.body;
    
    User.findOne({"email":req.body.email} , (err,user) => {
        if(err) return res.json({success: false,err: err})
        if(!user) return res.json({isAuth: false, message: "Auth failed , email not found !"});

    //    user.comparePassword(req.body.password, (err, isMatch) => {
    //        if(!isMatch) return res.json({
    //            isAuth:false,
    //            message:"Wrong passord"
    //        })
    //    })

    if(user) {
        bcrypt.compare(password, user.password, function(err, isMatch) {
            if(err) return res.status.json({success: false,err: err})
            // console.log(isMatch)
            if(!isMatch) return res.send({
                           isAuth:false,
                           message:"Wrong passord"
                       })
            
            if(isMatch){

                var token = jwt.sign(user._id.toHexString(), config.SECRET);

                user.token = token;
                user.save((err, user) => {
                    if(err) res.status(400).json({success: false,err: err})
                    res.cookie("auth", user.token).json({
                        isAuth:true,
                        id:user._id,
                        email:user.email
                    });
                })
            }
        });
            
        }
    })
})

// === UPDATE === //
app.post("/api/book_update", (req,res) => {
    Book.findByIdAndUpdate(req.body._id, req.body, {new:true} , (err, doc) => {
        if(err) return res.status(400).send(err)
        res.json({
            success:true,
            doc
        })
    })
})

// === DELETE === //
app.delete("/api/delete_book", (req,res) => {
    let id = req.query.id

    Book.findByIdAndRemove(id , (err,doc) => {
        if(err) return res.status(400).send(err)
        res.json(true);
    })
})

if(process.env.NODE_ENV === 'production'){
    const path = require('path');
    app.get("/*",(req,res)=> {
        res.sendFile(path.resolve(__dirname,"../client","build","index.html"))
    }) 
}



const PORT  = process.env.PORT || 3001;
app.listen(PORT , () => {
    console.log("Server running on port")
})